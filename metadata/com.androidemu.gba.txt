Categories:Games
License:GPLv3+
Web Site:http://gameboid.sf.net
Source Code:http://sf.net/p/gameboid/code
Issue Tracker:http://sf.net/p/gameboid/tickets

Auto Name:GameBoid
Summary:Nintendo Gameboy Advance emulator
Description:
This project is based on sources published by original GameBoid developer,
who in turn used code of gpSP.

To run this, you need a non-free BIOS file, which must be obtained
elsewhere.
.

Repo Type:git
Repo:git://git.code.sf.net/p/gameboid/code

Build:1.3.2,6
    commit=1.3.2
    subdir=GameBoid
    submodules=yes
    target=android-15
    patch=target_api_10.diff
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.3.2
Current Version Code:6

