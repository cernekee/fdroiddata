Categories:Reading
License:MIT
Web Site:https://github.com/dnet/adsdroid
Source Code:https://github.com/dnet/adsdroid
Issue Tracker:https://github.com/dnet/adsdroid/issues

Auto Name:ADSdroid
Summary:Get technical datasheets
Description:
Unofficial Android app for alldatasheet.com, allowing fast access to thousands
of PDFs for specifications about electronic items
.

Repo Type:git
Repo:https://github.com/dnet/adsdroid.git

Build:1.0,1
    commit=v1.0
    target=android-10
    extlibs=jsoup/jsoup-1.6.3.jar

Build:1.2,2
    commit=v1.2
    target=android-16
    extlibs=jsoup/jsoup-1.6.3.jar

Build:1.3,3
    commit=v1.3
    target=android-10
    extlibs=jsoup/jsoup-1.6.3.jar

Build:1.4,5
    commit=v1.4
    target=android-16
    extlibs=jsoup/jsoup-1.6.3.jar

Build:1.5,6
    commit=v1.5
    target=android-16
    srclibs=JSoup@jsoup-1.6.2
    prebuild=mkdir libs && \
        pushd $$JSoup$$ && \
        mvn package && \
        popd && \
        cp $$JSoup$$/target/*jar libs/

Build:1.5.1,7
    commit=v1.5.1
    target=android-16
    srclibs=JSoup@jsoup-1.6.2
    prebuild=mkdir libs && \
        pushd $$JSoup$$ && \
        mvn package && \
        popd && \
        cp $$JSoup$$/target/*jar libs/

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.5.1
Current Version Code:7

