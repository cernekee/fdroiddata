Categories:Office
License:GPLv3+
Web Site:http://mirakel.azapps.de
Source Code:https://github.com/MirakelX/mirakel-android
Issue Tracker:https://github.com/MirakelX/mirakel-android/issues
Donate:http://mirakel.azapps.de/help_us.html#donate
FlattrID:2188714

Name:Mirakel
Auto Name:Mirakel
Summary:Decentralized TODO list
Description:
Mirakel is a simple but powerful tool for managing your TODO-lists.
You can sync your lists with your own server! Features:

*  Manage your tasks in lists
*  Simple, but powerful User Interface
*  Tablet-UI
*  Nice little widget
*  Sort your tasks in different ways
*  Fully configurable
*  Notifications & Reminders
*  Easy Backup and Import
*  Import your tasks from Astrid
*  Sync with Taskwarrior
*  Sync with CalDAV

[http://mirakel.azapps.de/changelog.html Changelog]
.

Repo Type:git
Repo:https://github.com/MirakelX/mirakel-android.git

Build:1.0.0,2
    commit=v1.0

Build:1.0.1,3
    commit=v1.0.1.1

Build:1.1.0,4
    commit=v1.1.0

Build:1.1.1,5
    commit=v1.1.1

Build:2.0,6
    commit=v2.0

Build:2.1,7
    commit=v2.1

Build:2.1.1,8
    commit=v2.1.1

Build:2.1.2,10
    commit=v2.1.2

Build:2.1.3,16
    commit=v2.1.3

Build:2.1.4,17
    commit=v2.1.4

Build:2.2.0,29
    disable=old failing build
    commit=v2.2.0

Build:2.2.1,31
    commit=v2.2.1

Build:2.3.0,38
    commit=v2.3.0

Build:2.3.1,40
    commit=v2.3.1

Build:2.3.2,43
    commit=v2.3.2
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.4.0,50
    commit=v2.4.0
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.4.0R2,51
    commit=v2.4.0R2
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.4.1,52
    commit=v2.4.1
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.5-beta1,54
    commit=v2.5-beta1
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.5-beta2,55
    commit=v2.5-beta2
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.5,56
    commit=v2.5
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.6,61
    disable=bash step fails
    commit=v2.6
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Build:2.6.1,62
    commit=v2.6.1
    subdir=main
    gradle=fdroid
    prebuild=cd ../ && \
        cp build/*.gradle . && \
        rm -r appcompat/bin

Auto Update Mode:Version v%v
Update Check Mode:Tags ^v[0-9.]*$
Current Version:2.6.1
Current Version Code:62

