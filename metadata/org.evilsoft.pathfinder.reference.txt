AntiFeatures:UpstreamNonFree
Categories:Games
License:GPLv3
Web Site:http://legolas.org
Source Code:https://github.com/devonjones/PathfinderOpenReference
Issue Tracker:https://github.com/devonjones/PathfinderOpenReference/issues

Auto Name:Masterwork Tools: Pathfinder Open Reference
Summary:RPG reference manual
Description:
This reference gives you rapid access to every feat, spell, class, skill,
monster and rule in the full Pathfinder Roleplaying Game Reference Document.
With intuitive indexing and powerful search capability, the reference gets
you the information you need quickly so you can get back to the game.
Includes rules material from the Pathfinder Roleplaying Game Core Rulebook,
Advanced Player’s Guide, Ultimate Magic, Ultimate Combat, Ultimate Equipment,
GameMastery Guide, NPC Codex, Bestiary, Bestiary 2 and Bestiary 3.

The application has 31000 rule snippets allowing you to access over 20000
discrete rule topics. Including: 725 Feats, 1358 Spells, 1402 Monsters & NPCs,
45 Classes and a ton more.
The application allows you to hone in the passages that matter for what’s
going on in game now. Every title in every article is a link that lets you
just see/bookmark that section. It has a powerful and fast search lets you
find the rule you need, quickly.
It also allows you to create bookmark collections. You can bookmark passages
that you want quick access to as a GM, while creating other collections for
your characters, giving you quick access to the rules that applicable to that
character.
.

Repo Type:git
Repo:https://github.com/devonjones/PathfinderOpenReference.git

Build:1.0.18,19
    commit=1.0.19
    init=rm -f build.xml
    srclibs=1:ActionBarSherlock@4.4.0
    rm=libs/android-ant.jar,libs/libspen23_multiwindow.jar

Build:1.1.7,27
    commit=1.1.7
    init=rm -f build.xml
    srclibs=1:ActionBarSherlock@4.4.0
    rm=libs/android-ant.jar,libs/libspen23_multiwindow.jar

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2.2
Current Version Code:29

