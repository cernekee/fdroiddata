Categories:Multimedia
License:GPLv3
Web Site:https://github.com/tgwizard/sls
Source Code:https://github.com/tgwizard/sls
Issue Tracker:https://github.com/tgwizard/sls/issues

Auto Name:Simple Last.fm Scrobbler
Summary:Last.fm/Libre.fm Scrobbler
Description:
Scrobbling means submitting listening information to Last.fm and/or Libre.fm;
you can then get music recommendations and view your listening history and
statistics. The media player that you use must be able to work with the API
e.g. [[org.kreed.vanilla]] or [[jp.co.kayo.android.localplayer]]. Apps can
also hook into the Now-Playing broadcast: for example, [[com.jlyr]] can use it
to grab lyrics.

More info about scrobbling can be found on
[http://www.last.fm/help/faq?category=Scrobbling#201 Last.fm's FAQ].
.

Repo Type:git
Repo:https://github.com/tgwizard/sls.git

Build:1.4.3,30
    commit=v1.4.3
    rm=other/RhapsodyAndroidPreview.apk

Build:1.4.4,31
    commit=v1.4.4
    rm=other/RhapsodyAndroidPreview.apk

Build:1.4.5,32
    commit=3d97696f1dd232b97fd907e14305bb7ec5949864
    rm=other/RhapsodyAndroidPreview.apk

Build:1.4.6,33
    commit=v1.4.6

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.4.6
Current Version Code:33

