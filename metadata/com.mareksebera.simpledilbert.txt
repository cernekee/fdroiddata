Provides:com.mareksebera.dilbert
Categories:Reading
License:Apache2
Web Site:http://smarek.github.io/Simple-Dilbert
Source Code:https://github.com/smarek/Simple-Dilbert
Issue Tracker:https://github.com/smarek/Simple-Dilbert/issues
FlattrID:1134425
Bitcoin:1FpGN3kcZ3GQsaJgBa8rxGRJjBjJavu78g

Auto Name:Simple Dilbert
Summary:Comic strip reader
Description:
* Select image quality
* Make favorite strips
* Caching, so you won't download single strip twice
* All comics strips since 16.4.1989
* Supports both large and small screens
* Supports all device orientations
.

Repo Type:git
Repo:https://github.com/smarek/Simple-Dilbert

Build:2.3,15
    commit=v2.3
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.3.1,ImageLoader@v1.8.5,PhotoView@05b1c2aae5985
    prebuild=cp -f libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i 's@\(reference.2=\).*@\1$$ImageLoader$$@' project.properties && \
        sed -i 's@\(reference.3=\).*@\1$$PhotoView$$@' project.properties

Build:3.6,29
    commit=v3.6
    update=.,libraries/FileExplorer
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.3.1,ImageLoader@077a22f88,PhotoView@05b1c2aae5985
    prebuild=cp -f libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i 's@\(reference.2=\).*@\1$$ImageLoader$$@' project.properties && \
        sed -i 's@\(reference.3=\).*@\1$$PhotoView$$@' project.properties && \
        sed -i 's@\(reference.1=\).*@\1../../../srclib/ActionBarSherlock/actionbarsherlock@' libraries/FileExplorer/project.properties

Build:3.7,30
    commit=v3.7
    update=.,libraries/FileExplorer
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.4.0,ImageLoader@077a22f88,PhotoView@05b1c2aae5985
    prebuild=cp -f libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        sed -i 's@\(reference.2=\).*@\1$$ImageLoader$$@' project.properties && \
        sed -i 's@\(reference.3=\).*@\1$$PhotoView$$@' project.properties && \
        sed -i 's@\(reference.1=\).*@\1../../../srclib/ActionBarSherlock/actionbarsherlock@' libraries/FileExplorer/project.properties

Build:3.8,31
    commit=v3.8
    submodules=yes
    update=.,libraries/ActionBarSherlock/actionbarsherlock,libraries/Android-Universal-Image-Loader/library,libraries/FileExplorer,libraries/PhotoView/library
    extlibs=android/android-support-v4.jar
    prebuild=cp -f libs/android-support-v4.jar libraries/ActionBarSherlock/actionbarsherlock/libs/ && \
        rm -f libraries/Android-Universal-Image-Loader/downloads/*.apk

Build:3.9,32
    commit=v3.9
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.9
Current Version Code:32

