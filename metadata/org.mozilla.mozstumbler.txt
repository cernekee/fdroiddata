Categories:Navigation
License:MPL2
Web Site:https://location.services.mozilla.com
Source Code:https://github.com/mozilla/MozStumbler
Issue Tracker:https://github.com/mozilla/MozStumbler/issues

Auto Name:MozStumbler
Summary:Data gathering for Mozilla Location Service
Description:
Android Stumbler for Mozilla Location Service.  This allows you
to contribute a mapping from Cell towers and WiFi access points
to a GPS location. This will be used for location services when
no GPS is available, similar to OpenWLANMap.
.

Repo Type:git
Repo:https://github.com/mozilla/MozStumbler.git

Build:0.8.2,20
    commit=32d4102
    gradle=yes
    prebuild=sed -i -e 's/debuggable true/debuggable false/' -e '/task wrapper/,/^}$/d' build.gradle

Build:0.8.3,21
    commit=v0.8.3
    gradle=yes
    prebuild=sed -i -e 's/debuggable true/debuggable false/' build.gradle

Build:0.10.0,23
    commit=v0.10.0
    gradle=yes
    prebuild=sed -i -e 's/debuggable true/debuggable false/' build.gradle

Build:0.10.1,24
    commit=v0.10.1
    gradle=yes
    prebuild=sed -i -e 's/debuggable true/debuggable false/' build.gradle

Build:0.10.2,25
    commit=v0.10.2
    gradle=yes

Build:0.11.0,26
    commit=v0.11.0
    gradle=yes

Build:0.12.0,27
    commit=v0.12.0
    gradle=yes

Build:0.12.1,28
    commit=v0.12.1
    gradle=yes

Build:0.13.0,29
    commit=v0.13.0
    gradle=yes

Build:0.14.0,30
    commit=v0.14.0
    gradle=yes

Build:0.15.0,31
    commit=v0.15.0
    gradle=yes

Build:0.17.0,33
    commit=v0.17.0
    gradle=yes

Build:0.18.0,34
    commit=v0.18.0
    gradle=yes

Build:0.19.0,35
    commit=v0.19.0
    gradle=yes

Build:0.20.0,36
    commit=v0.20.0
    gradle=yes

Build:0.20.3,39
    commit=v0.20.3
    gradle=yes

Build:0.20.4,40
    commit=v0.20.4
    gradle=yes

Build:0.20.5,41
    commit=v0.20.5
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.20.5
Current Version Code:41

